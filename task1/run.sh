#!/usr/bin/env bash

hadoop jar ../hadoop-streaming-2.7.3.jar \
-D  mapred.output.key.comparator.class=org.apache.hadoop.mapred.lib.KeyFieldBasedComparator \
-D  mapreduce.map.output.field.seperator=" " \
-D  mapreduce.partition.keycomparator.options="-k1 -k2n" \
-D  num.key.fields.for.partition=1 \
-D  stream.num.map.output.key.field=2 \
-input /data/assignments/ex2/part1/large/ \
-output /user/s1308424/ex2/part1/ \
-mapper mapper.py \
-file mapper.py \
-combiner combiner.py \
-file combiner.py \
-reducer reducer.py \
-file reducer.py
