#!/usr/bin/env bash

hadoop jar ../hadoop-streaming-2.7.3.jar \
-D  mapred.output.key.comparator.class=org.apache.hadoop.mapred.lib.KeyFieldBasedComparator \
-D  mapreduce.partition.keypartitioner.options=-k1,1 \
-D  mapreduce.partition.keycomparator.options=-k1,1nr \
-D  stream.num.map.output.key.field=1 \
-partitioner org.apache.hadoop.mapred.lib.KeyFieldBasedPartitioner \
-input /data/assignments/ex2/part2/stackSmall.txt \
-output /user/s1308424/ex2/part3/ \
-mapper mapper.py \
-file mapper.py 
#-combiner combiner.py \
#-file combiner.py 
#-reducer reducer.py \
#-file reducer.py
